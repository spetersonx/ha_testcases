# Sensor config file for HA Test Harness
#
# This file describes a generic 500mV/G sensor vertically mounted on the Shaker.

# ADC to Volts conversion factor
adc2voltage 39112

# Sensor Calibration (mV/G): 0, or value from data sheet
sensorCalibration 0

# Nominal Sensitivity (mV/G): [10, 100, 500]
nominalSensitivity 500

# Sensor Orientation: [Vertical, Horizontal, Axial]
sensorOrientation Vertical

# Sensor angle with reference to chart, in degrees: [0-359]
sensorAngle 0

# Mount position on the equipment
mountPosition 1  
